package br.senai.sp.informatica.objetos.exemplos.validacao.escola;

import java.util.List;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class AlunoModel extends AbstractTableModel {
	private String[] titulo = {"Nome", "Matrícula", "E-Mail" };
	private List<Aluno> lista;
	
	public AlunoModel(List<Aluno> lista) {
		this.lista = lista;
	}
	
	public void adiciona(Aluno obj) {
		lista.add(obj);
		fireTableDataChanged();
	}
	
	public void remove(int linha) {
		lista.remove(linha);
		fireTableDataChanged();
	}
	
	@Override
	public int getRowCount() {
		return lista.size();
	}

	@Override
	public int getColumnCount() {
		return titulo.length;
	}

	@Override
	public String getColumnName(int col) {
		return titulo[col];
	}

	@Override
	public Object getValueAt(int linha, int col) {
		Aluno obj = lista.get(linha);
		Object valor = null;
		
		switch (col) {
		case 0:
			valor = obj.getNome();
			break;
		case 1:
			valor = obj.getMatricula();
			break;
		case 2:
			valor = obj.getEmail();
		}
		return valor;
	}

}
