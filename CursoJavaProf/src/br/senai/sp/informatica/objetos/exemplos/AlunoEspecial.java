package br.senai.sp.informatica.objetos.exemplos;

import java.util.ArrayList;
import java.util.List;

public class AlunoEspecial extends Aluno {
	private List<Double> notas = new ArrayList<>();

	public List<Double> getNotas() {
		return notas;
	}
	
	public void setNotas(List<Double> notas) {
		this.notas = notas;
	}

	
	@Override
	public void adicionaNota(double nota) {
		notas.add(nota);
	}
}
