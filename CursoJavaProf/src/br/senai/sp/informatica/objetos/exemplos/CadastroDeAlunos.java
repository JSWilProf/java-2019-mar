package br.senai.sp.informatica.objetos.exemplos;

import static br.senai.sp.informatica.lib.SwUtil.escreva;
import static br.senai.sp.informatica.lib.SwUtil.leInteiro;
import static br.senai.sp.informatica.lib.SwUtil.leReal;
import static br.senai.sp.informatica.lib.SwUtil.leTexto;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class CadastroDeAlunos {
	public static void main(String[] args) {
		Curso oCurso = new Curso();
		oCurso.setNome(leTexto("Informe o Nome do Curso"));
		oCurso.setProfessor(leTexto("Informe o nome do Prof."));
		oCurso.setQtdHoras(leInteiro("Informe a qtd de horas"));
		oCurso.setAlunos(new ArrayList<>());

		int opcao = JOptionPane.showOptionDialog(null, "Selecione o Tipo", "Escolha o Aluno", 
				JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, 
				new String[] {"Especial","Padrão","Sair"}, "Padrão");
				
		while(opcao != 2) {
			// Criar um Aluno
			Aluno umAluno;
			
			if(opcao == 1) {
				umAluno = new AlunoPadrao();
			} else {
				umAluno = new AlunoEspecial();
			}
			cadastra(umAluno, oCurso);
			
			opcao = JOptionPane.showOptionDialog(null, "Selecione o Tipo", "Escolha o Aluno", 
					JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, 
					new String[] {"Especial","Padrão","Sair"}, "Padrão");
		}
		
		escreva(oCurso.toString());
	}
	
	public static void cadastra(Aluno umAluno, Curso oCurso) {
		// Atribuir o Nome
		umAluno.setNome(leTexto("Informe o Nome do Aluno"));
		// Atribuir suas Notas
		for (int i = 0; i < 4; i++) 
			umAluno.adicionaNota(leReal(i+1,"ª nota"));
		
		// Matricular no Curso
		umAluno.setCurso(oCurso);
		
		oCurso.getAlunos().add(umAluno);
	}
}
