package br.senai.sp.informatica.objetos.exemplos;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import br.senai.sp.informatica.lib.SwUtil;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JFormattedTextField;

@SuppressWarnings("serial")
public class GestaoDoParqueAquatico extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JLabel lblNome;
	private JTextField tfNome;
	private JLabel lblEstaoDoAno;
	private JComboBox<Estacoes> comboBox;
	private JButton btnSalvar;
	private JButton btnSair;

	private List<TemporadaDeFerias> lista = new ArrayList<>();
	private JButton btnListar;
	private JLabel lblIncioDaTemporada;
	private JFormattedTextField tfInicio;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GestaoDoParqueAquatico frame = new GestaoDoParqueAquatico();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public GestaoDoParqueAquatico() {
		setResizable(false);
		setTitle("Getão do Parque Aquático");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 421, 259);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		lblNome = new JLabel("Nome");
		
		tfNome = new JTextField();
		tfNome.setColumns(10);
		
		lblEstaoDoAno = new JLabel("Estação do Ano");
		
		comboBox = new JComboBox<>();
		comboBox.setModel(new DefaultComboBoxModel<Estacoes>(Estacoes.values()));
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(this);
		
		btnSair = new JButton("Sair");
		btnSair.addActionListener(this);
		
		btnListar = new JButton("Listar");
		btnListar.addActionListener(this);
		
		lblIncioDaTemporada = new JLabel("Início da Temporada");
		
		tfInicio = new JFormattedTextField(SwUtil.criaMascara("##/##/####"));
		tfInicio.setColumns(10);
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnSalvar)
							.addGap(93)
							.addComponent(btnListar)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(btnSair))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblNome)
							.addGap(18)
							.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, 334, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
							.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
								.addComponent(lblIncioDaTemporada)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(tfInicio))
							.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
								.addComponent(lblEstaoDoAno)
								.addGap(18)
								.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNome))
					.addGap(27)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEstaoDoAno)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblIncioDaTemporada)
						.addComponent(tfInicio, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 75, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSalvar)
						.addComponent(btnSair)
						.addComponent(btnListar))
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
		
		getRootPane().setDefaultButton(btnSalvar);
	}

	public void actionPerformed(ActionEvent evento) {
		Object botao = evento.getSource();
		
		if(botao.equals(btnSalvar)) {
			try {
				TemporadaDeFerias obj = new TemporadaDeFerias();
				obj.setNome(tfNome.getText());
				obj.setEstacao((Estacoes)comboBox.getSelectedItem());
				
				String data = tfInicio.getText();
				LocalDate inicio = LocalDate.parse(data, 
						DateTimeFormatter.ofPattern("dd/MM/yyyy")
						.withResolverStyle(ResolverStyle.STRICT));
				obj.setInicio(inicio);
				
				lista.add(obj);
				
				SwUtil.limpa(this);
				tfNome.requestFocus();
			} catch (DateTimeParseException ex) {
				JOptionPane.showMessageDialog(this, "Data de Início é inválida");
			}
		} else if(botao.equals(btnListar)){
			String msg = "Lista das Estações Catalogadas\n\n";
			for (TemporadaDeFerias temporadaDeFerias : lista) {
				msg += temporadaDeFerias + "\n";
			}
			JOptionPane.showMessageDialog(this, msg);
		} else {
			System.exit(0);
			
		}
	
	}
}
