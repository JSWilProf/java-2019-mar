package br.senai.sp.informatica.objetos.exemplos.jogos;

import static br.senai.sp.informatica.lib.SwUtil.escreva;
import static br.senai.sp.informatica.lib.SwUtil.leTexto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class CatalogoDeJogos {
	public static void main(String[] args) {
		//----------------- Declaração do cadastro ----------
		List<Jogo> catalogo = new ArrayList<>();
		//---------------------------------------------------
		
		//----------------- A ação de Salvar ----------------
		String nome = leTexto("Informe o Nome");
		while(!nome.equalsIgnoreCase("fim")) {
			Jogo jogo = new Jogo();
			jogo.setNome(nome);
			jogo.setGenero(leTexto("Informe o Gênero"));
			
			catalogo.add(jogo);
			
			nome = leTexto("Informe o Nome");
		}
		//---------------------------------------------------

		//----------------- A ação de Listar ----------------
		String msg = "Catálogo de Jogos\n\n" +
				catalogo.stream()
					.map(Jogo::toString)
					.collect(Collectors.joining("\n"));

//		String msg2 = "Catálogo de Jogos\n\n";
//		for (Jogo jogo : catalogo) {
//			msg2 += jogo + "\n";
//		}
		escreva(msg);
		//---------------------------------------------------
	}
}
