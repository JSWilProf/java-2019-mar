package br.senai.sp.informatica.objetos.exemplos.fonts;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

@SuppressWarnings({"serial", "rawtypes", "unchecked"})
public class BuscaFonts extends JFrame implements ItemListener {

	private JPanel contentPane;
	private JLabel lbMinusculas;
	private JLabel lbMaiusculas;
	private JComboBox comboBox;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BuscaFonts frame = new BuscaFonts();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public BuscaFonts() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 956, 194);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		lbMinusculas = new JLabel("abcdefghijklmnopqrstuvxz");
		
		lbMaiusculas = new JLabel("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
		
		comboBox = new JComboBox();
		comboBox.addItemListener(this);
		comboBox.setModel(new DefaultComboBoxModel(FreeFonts.values()));
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lbMinusculas, GroupLayout.DEFAULT_SIZE, 783, Short.MAX_VALUE)
						.addComponent(lbMaiusculas, GroupLayout.DEFAULT_SIZE, 783, Short.MAX_VALUE)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(22)
					.addComponent(lbMinusculas)
					.addGap(18)
					.addComponent(lbMaiusculas)
					.addPreferredGap(ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
					.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}

	@Override
	public void itemStateChanged(ItemEvent ev) {
		try {
			Font font = ((FreeFonts)comboBox.getSelectedItem()).getFont();
			lbMinusculas.setFont(font);
			lbMaiusculas.setFont(font);
		} catch(Exception ex) {
			JOptionPane.showMessageDialog(this, "Font não encontrada");
		}
	} 
}
