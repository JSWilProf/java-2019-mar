package br.senai.sp.informatica.objetos.exemplos.validacao.escola;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import br.senai.sp.informatica.lib.SwUtil;
import br.senai.sp.informatica.lib.optional.StatusValidador;
import br.senai.sp.informatica.lib.optional.TableVerificaInteiro;
import br.senai.sp.informatica.lib.optional.TableVerificaRegex;
import br.senai.sp.informatica.lib.optional.VerificaCellEditor;
import br.senai.sp.informatica.lib.optional.VerificaInteiro;
import br.senai.sp.informatica.lib.optional.VerificaRegex;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

@SuppressWarnings("serial")
public class CadAlunoValidador extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JLabel lblNome;
	private JTextField tfNome;
	private JLabel lblMatrcula;
	private JFormattedTextField tfMatricula;
	private JLabel lblEmail;
	private JTextField tfEmail;
	private JScrollPane scrollPane;
	private JTable table;
	private JButton btnInserir;
	private JButton btnRemover;
	private JButton btnSair;
	
	private List<Aluno> lista = new ArrayList<Aluno>();
	private AlunoModel model = new AlunoModel(lista);
	private JLabel errNome;
	private JLabel errMatricula;
	private JLabel errEmail;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadAlunoValidador frame = new CadAlunoValidador();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public CadAlunoValidador() {
		setTitle("Cadastro de Alunos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 580, 437);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
				
		errNome = new JLabel(" ");
		
		errMatricula = new JLabel(" ");
		
		errEmail = new JLabel(" ");
		
		lblNome = new JLabel("Nome");
		
		tfNome = new JTextField();
		tfNome.setColumns(10);
		tfNome.setInputVerifier(new VerificaRegex(errNome, ".{3,}( .{2,})+", true, "Nome Inválido"));
		
		lblMatrcula = new JLabel("Matr\u00EDcula");
		
		tfMatricula = new JFormattedTextField(NumberFormat.getIntegerInstance());
		tfMatricula.setFocusLostBehavior(JFormattedTextField.COMMIT);
		tfMatricula.setColumns(10);
		tfMatricula.setInputVerifier(new VerificaInteiro(errMatricula, true));
		
		lblEmail = new JLabel("E-Mail");
		
		tfEmail = new JTextField();
		tfEmail.setInputVerifier(new VerificaRegex(errEmail, "([\\w-]{2,}\\.){0,}[\\w-]{3,}@[\\w-]{3,}(\\.\\w{2,}){1,2}", true));
		tfEmail.setColumns(10);
		
		scrollPane = new JScrollPane();
		
		btnInserir = new JButton("Inserir");
		btnInserir.addActionListener(this);
		
		btnRemover = new JButton("Remover");
		btnRemover.setVerifyInputWhenFocusTarget(false);
		btnRemover.addActionListener(this);
		
		btnSair = new JButton("Sair");
		btnSair.setVerifyInputWhenFocusTarget(false);
		btnSair.addActionListener(this);
		
		table = new JTable();
		table.setModel(model);
		table.setAutoCreateRowSorter(true);
		
		// Parametriza as colunas de edição do JTable
		
		table.getColumnModel().getColumn(0).setCellEditor(
				new VerificaCellEditor(new TableVerificaRegex(".{3,}( .{2,})+")));
		
		
		JFormattedTextField cellTf = new JFormattedTextField(NumberFormat.getIntegerInstance());
		table.getColumnModel().getColumn(1).setCellEditor(
				new VerificaCellEditor(cellTf, new TableVerificaInteiro()));
		
		
		table.getColumnModel().getColumn(2).setCellEditor(
				new VerificaCellEditor(new TableVerificaRegex("([\\w-]{2,}\\.){0,}[\\w-]{3,}@[\\w-]{3,}(\\.\\w{2,}){1,2}")));

		scrollPane.setViewportView(table);
		
		getRootPane().setDefaultButton(btnInserir);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(6)
							.addComponent(lblNome)
							.addGap(6)
							.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, 305, GroupLayout.PREFERRED_SIZE)
							.addGap(1)
							.addComponent(errNome, GroupLayout.PREFERRED_SIZE, 184, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(6)
							.addComponent(lblMatrcula)
							.addGap(6)
							.addComponent(tfMatricula, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(errMatricula, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(6)
							.addComponent(lblEmail)
							.addGap(6)
							.addComponent(tfEmail, GroupLayout.PREFERRED_SIZE, 301, GroupLayout.PREFERRED_SIZE)
							.addGap(1)
							.addComponent(errEmail, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE))
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 569, Short.MAX_VALUE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnInserir)
							.addGap(6)
							.addComponent(btnRemover)
							.addGap(6)
							.addComponent(btnSair)))
					.addGap(1))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(6)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(6)
							.addComponent(lblNome))
						.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(6)
							.addComponent(errNome)))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(6)
							.addComponent(lblMatrcula))
						.addComponent(tfMatricula, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(6)
							.addComponent(errMatricula)))
					.addGap(21)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(6)
							.addComponent(lblEmail))
						.addComponent(tfEmail, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(6)
							.addComponent(errEmail)))
					.addGap(35)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
					.addGap(36)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnInserir)
						.addComponent(btnRemover)
						.addComponent(btnSair))
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
		
	}

	public void actionPerformed(ActionEvent ev) {
		Object botao = ev.getSource();
		
		if(botao.equals(btnInserir)) {
			StatusValidador verificacao = SwUtil.verificaStatus(this);
			
			if(verificacao.equals(StatusValidador.OK)) {
				String nome = tfNome.getText();
				
				Number temp = (Number)tfMatricula.getValue();
				int matr = temp.intValue();
						
				String email = tfEmail.getText();
			
				Aluno obj = new Aluno();
				obj.setNome(nome);
				obj.setMatricula(matr);
				obj.setEmail(email);
			
				model.adiciona(obj);
			
				SwUtil.limpa(this);
				tfNome.requestFocus();
			} else if(verificacao.equals(StatusValidador.EM_BRANCO)) {
				JOptionPane.showMessageDialog(this, "Existem campos em BRANCO");
			} else {
				JOptionPane.showMessageDialog(this, "Existem campos com ERRO");
			}
		} else if(botao.equals(btnRemover)) {
			int linha = table.getSelectedRow();
			
			if(linha > -1) {
				if(table.getRowSorter() != null) 
					linha = table.getRowSorter().convertRowIndexToModel(linha);
				
				model.remove(linha);
			} else {
				JOptionPane.showMessageDialog(this, "Uma linha na tabela deve ser selecionada");
			}
		} else {
			System.exit(0);
		}
	}
}
