package br.senai.sp.informatica.objetos.exemplos.fonts;

import java.awt.Font;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum FreeFonts {
	DEFAULT("Dialog", 12),
	ALHAMBRA("alhambra", 20),
	BLAZED("blazed", 24),
	FANCYHEART("fancyheart", 30),
	GRUNJA("grunja", 28),
	POZOFOUT("pozofour", 36),
	SHANGRILA("shangrila", 32);
	
	private String nome;
	private Number tamanho;
	
	public Font getFont() throws Exception {
		if(nome.equals("Dialog"))
			return new Font(nome, Font.BOLD, tamanho.intValue());
		
		return Font.createFont(Font.TRUETYPE_FONT, 
				FreeFonts.class.getResourceAsStream(nome + ".ttf")).deriveFont(tamanho.floatValue());
	}
}
