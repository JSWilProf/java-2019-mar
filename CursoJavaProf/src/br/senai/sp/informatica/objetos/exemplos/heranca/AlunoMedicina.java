package br.senai.sp.informatica.objetos.exemplos.heranca;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AlunoMedicina extends Aluno {
	private static final long serialVersionUID = -103001454452615077L;

	private List<String> instrumentos = new ArrayList<>();

	@Override
	public String toString() {
		String texto =  " Instrumentos:\n";
		
		for (String instrumento : instrumentos) {
			texto += instrumento + "\n";
		}
		
		return super.toString() + texto;
	}

	@Override
	public String getTipo() {
		return "Medicina";
	}
}
