package br.senai.sp.informatica.objetos.exemplos;

public class AlunoPadrao extends Aluno {
	private double[] notas = new double[4];
	private int indice = 0; 

	public double[] getNotas() {
		return notas;
	}

	public void setNotas(double[] notas) {
		this.notas = notas;
	}

	
	@Override
	public void adicionaNota(double nota) {
		if(indice <= 3) {
			notas[indice] = nota;
			indice++;
		}
	}
	
}
