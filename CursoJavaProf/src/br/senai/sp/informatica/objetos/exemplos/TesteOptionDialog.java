package br.senai.sp.informatica.objetos.exemplos;

import javax.swing.JOptionPane;

public class TesteOptionDialog {

	public static void main(String[] args) {
		// TODO Stub de método gerado automaticamente
		int opcao = JOptionPane.showOptionDialog(null, "Selecione o Tipo", "Escolha o Aluno", 
				JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, 
				new String[] {"Especial","Padrão","Sair"}, "Padrão");
		
		JOptionPane.showMessageDialog(null, opcao);
	}

}
