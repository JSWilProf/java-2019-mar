package br.senai.sp.informatica.objetos.exemplos.validacao.escola;

import lombok.Data;

@Data
public class Aluno {
	private String nome;
	private int matricula;
	private String email;

	@Override
	public String toString() {
		return "nome: " + nome + " matricula: " + matricula + " email: "
				+ email;
	}
}
