package br.senai.sp.informatica.objetos.exemplos;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TemporadaDeFerias {
	private String nome;
	private LocalDate inicio;
	private Estacoes estacao;
	
	@Override
	public String toString() {
		return "nome: " + nome + " inicio: " + inicio + " estacao: " + estacao;
	}
}
