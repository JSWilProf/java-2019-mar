package br.senai.sp.informatica.objetos.exemplos;

public enum Estacoes {
	PRIMAVERA {
		@Override
		public String toString() {
			return "Primavera";
		}
	}, 
	VERAO {
		@Override
		public String toString() {
			return "Verão";
		}
	}, 
	OUTONO {
		@Override
		public String toString() {
			return "Outono";
		}
	}, 
	INVERNO {
		@Override
		public String toString() {
			return "Inverno";
		}
	};
}
