package br.senai.sp.informatica.objetos.exemplos;

import javax.swing.JOptionPane;

public class ExemploEnumeracao {
	public static void main(String[] args) {
		TemporadaDeFerias temp1 = TemporadaDeFerias.builder()
				.nome("Fériad de Verão")
				.estacao(Estacoes.VERAO)
				.build();
		
		JOptionPane.showMessageDialog(null, temp1);
	}
}

