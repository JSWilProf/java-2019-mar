package br.senai.sp.informatica.objetos.exemplos;

import java.util.List;

import lombok.Data;

@Data
public class Curso {
	private String nome;
	private String professor;
	private int qtdHoras;
	private List<Aluno> alunos;
	
	@Override
	public String toString() {
		String msg = "Curso: " + nome + "\nProf.: " + professor + 
				"\nNº Horas: " + qtdHoras + "\n";
	
		for (Aluno aluno : alunos) {
			msg += aluno + "\n";
		}
		return msg;
	}
	
	
}
