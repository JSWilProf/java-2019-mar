package br.senai.sp.informatica.objetos.exemplos.arquivos;

import java.awt.FileDialog;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class TesteFileDialog {
	public static void main(String[] args) {
		FileDialog fd = new FileDialog((JDialog) null, "Abrir", FileDialog.LOAD);
		fd.setFilenameFilter((File dir, String name) -> name.contains(".txt") ? true : false);
		fd.setVisible(true);
		
		String nome = fd.getDirectory() + fd.getFile();
		
		try (BufferedReader in = new BufferedReader(new FileReader(nome));) {

			String linha = in.readLine();
			String msg = "Conteúdo do Arquivo:\n\n";
			
			while (linha != null) {
				msg += linha + "\n";
				linha = in.readLine();
			}
			JOptionPane.showMessageDialog(null, msg);
		} catch (FileNotFoundException ex) {
			JOptionPane.showMessageDialog(null, "O Arquivo " + nome + "não foi encontrado");
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, "Problemas na leitura do arquivo: " + nome);
		}
	}
}
