package br.senai.sp.informatica.objetos.exemplos.jogos;

import lombok.Data;

@Data
public class Jogo {
	private String nome;
	private String genero;
	
	@Override
	public String toString() {
		return "Nome: " + nome + " Gênero: " + genero;
	}
}
