package br.senai.sp.informatica.objetos.exemplos.heranca;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class AlunoEngenharia extends Aluno {
	private static final long serialVersionUID = 2781335937619537068L;

	private String calculadora;

	@Override
	public String toString() {
		return super.toString() + " Calculadora: " + calculadora;
	}

	@Override
	public String getTipo() {
		return "Engenharia";
	}
}
