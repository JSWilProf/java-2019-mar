package br.senai.sp.informatica.objetos.exemplos.heranca;

import java.awt.EventQueue;
import java.awt.FileDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import br.senai.sp.informatica.lib.SwUtil;


@SuppressWarnings("serial")
public class CadAunos extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JLabel lbNome;
	private JTextField tfNome;
	private JLabel lblMatricula;
	private JTextField tfMatricula;
	private JLabel lblEnd;
	private JTextField tfEnd;
	private JTabbedPane tabbedPane;
	private JPanel pnMedicina;
	private JPanel pnEngenharia;
	private JLabel lninstrumento;
	private JTextField tfInstrumento;
	private JLabel lbCalculadora;
	private JTextField tfCalculadora;
	private JButton btnSalvar;
	private JButton btnListar;
	private JButton btnSair;
	private JLabel lbFormatura;
	private JFormattedTextField tfFormatura;

	private DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		
	private List<Aluno> alunos = new ArrayList<>();
	private JButton btnGravar;
	private JButton btnCarregar;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadAunos frame = new CadAunos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public CadAunos() {
		setTitle("Cadastro de Alunos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 540, 316);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		lbNome = new JLabel("Nome");
		
		tfNome = new JTextField();
		tfNome.setColumns(10);
		
		lblMatricula = new JLabel("Matricula");
		
		tfMatricula = new JTextField();
		tfMatricula.setColumns(10);
		
		lbFormatura = new JLabel("Date de Formatura");
		tfFormatura = new JFormattedTextField(SwUtil.criaMascara("##/##/####"));

		lblEnd = new JLabel("End.");
		
		tfEnd = new JTextField();
		tfEnd.setColumns(10);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(this);
		
		btnListar = new JButton("Listar");
		btnListar.addActionListener(this);
		
		btnSair = new JButton("Sair");
		btnSair.addActionListener(this);
		
		btnGravar = new JButton("Gravar");
		btnGravar.addActionListener(this);
		
		btnCarregar = new JButton("Carregar");
		btnCarregar.addActionListener(this);
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 485, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lbNome)
							.addGap(18)
							.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, 378, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblMatricula)
							.addGap(18)
							.addComponent(tfMatricula, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(lbFormatura)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(tfFormatura, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblEnd)
							.addGap(18)
							.addComponent(tfEnd, GroupLayout.DEFAULT_SIZE, 473, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnSalvar)
							.addGap(31)
							.addComponent(btnListar)
							.addGap(33)
							.addComponent(btnGravar)
							.addPreferredGap(ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
							.addComponent(btnCarregar)
							.addGap(18)
							.addComponent(btnSair)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lbNome)
						.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblMatricula)
						.addComponent(tfMatricula, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lbFormatura)
						.addComponent(tfFormatura, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEnd)
						.addComponent(tfEnd, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSalvar)
						.addComponent(btnSair)
						.addComponent(btnListar)
						.addComponent(btnGravar)
						.addComponent(btnCarregar))
					.addContainerGap())
		);
		
		pnMedicina = new JPanel();
		tabbedPane.addTab("Medicina", null, pnMedicina, null);
		
		lninstrumento = new JLabel("Instrumento");
		
		tfInstrumento = new JTextField();
		tfInstrumento.setColumns(10);
		GroupLayout gl_pnMedicina = new GroupLayout(pnMedicina);
		gl_pnMedicina.setHorizontalGroup(
			gl_pnMedicina.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnMedicina.createSequentialGroup()
					.addGap(19)
					.addComponent(lninstrumento)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(tfInstrumento, GroupLayout.PREFERRED_SIZE, 326, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(30, Short.MAX_VALUE))
		);
		gl_pnMedicina.setVerticalGroup(
			gl_pnMedicina.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnMedicina.createSequentialGroup()
					.addGap(25)
					.addGroup(gl_pnMedicina.createParallelGroup(Alignment.BASELINE)
						.addComponent(lninstrumento)
						.addComponent(tfInstrumento, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(27, Short.MAX_VALUE))
		);
		pnMedicina.setLayout(gl_pnMedicina);
		
		pnEngenharia = new JPanel();
		tabbedPane.addTab("Engenharia", null, pnEngenharia, null);
		
		lbCalculadora = new JLabel("Calculadora");
		
		tfCalculadora = new JTextField();
		tfCalculadora.setColumns(10);
		GroupLayout gl_pnEngenharia = new GroupLayout(pnEngenharia);
		gl_pnEngenharia.setHorizontalGroup(
			gl_pnEngenharia.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnEngenharia.createSequentialGroup()
					.addGap(24)
					.addComponent(lbCalculadora)
					.addGap(18)
					.addComponent(tfCalculadora, GroupLayout.PREFERRED_SIZE, 322, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(26, Short.MAX_VALUE))
		);
		gl_pnEngenharia.setVerticalGroup(
			gl_pnEngenharia.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_pnEngenharia.createSequentialGroup()
					.addContainerGap(28, Short.MAX_VALUE)
					.addGroup(gl_pnEngenharia.createParallelGroup(Alignment.BASELINE)
						.addComponent(lbCalculadora)
						.addComponent(tfCalculadora, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(24))
		);
		pnEngenharia.setLayout(gl_pnEngenharia);
		contentPane.setLayout(gl_contentPane);
	}

	@SuppressWarnings("unchecked")
	public void actionPerformed(ActionEvent evento) {
		Object botao = evento.getSource();
		
		if(botao.equals(btnSalvar)) {
			
			try {
			
			Object aba = tabbedPane.getSelectedComponent();
			if(aba.equals(pnMedicina)) {
				AlunoMedicina aluno = new AlunoMedicina();
				aluno.setMatricula(Integer.parseInt(tfMatricula.getText()));
				aluno.setFormatura(LocalDate.parse(tfFormatura.getText(), fmt));
				aluno.setNome(tfNome.getText());
				aluno.setEndereco(tfEnd.getText());
				aluno.getInstrumentos().add(tfInstrumento.getText());
//				List<String> instrumentos = aluno.getInstrumentos();
//				instrumentos.add(tfInstrumento.getText());
				
				alunos.add(aluno);
			} else {  // Engenharia
				AlunoEngenharia aluno = new AlunoEngenharia();
				aluno.setMatricula(Integer.parseInt(tfMatricula.getText()));
				aluno.setFormatura(LocalDate.parse(tfFormatura.getText(), fmt));
				aluno.setNome(tfNome.getText());
				aluno.setEndereco(tfEnd.getText());
				aluno.setCalculadora(tfCalculadora.getText());
				
				alunos.add(aluno);
			}
			SwUtil.limpa(this);
			tfNome.requestFocus();
			
			} catch (DateTimeParseException ex) {
				JOptionPane.showMessageDialog(this, "Data de Formatura é inválida");
			}
			
		} else if(botao.equals(btnListar)) {
			int opcao = JOptionPane.showOptionDialog(this, "Selecione o Tipo de Ordenação", 
					"Relatório", JOptionPane.DEFAULT_OPTION, 
					JOptionPane.QUESTION_MESSAGE, null, 
					new String[] {"por Nome", "por Tipo e Nome"}, "por Nome");
			
			if(opcao <= 0) {
				// Ordena pelo nome do Aluno
				Collections.sort(alunos, Comparator.comparing(Aluno::getNome));
			} else {
				// Ordena por Tipo e por Nome
				Collections.sort(alunos, 
					Comparator.comparing(Aluno::getTipo).thenComparing(Aluno::getNome)  );
			}
			
			String msg = "Cadastro de Alunos\n\n";
			for (Aluno aluno : alunos) {
				msg += aluno + "\n";
			}
			JOptionPane.showMessageDialog(this, msg);
		} else if(botao.equals(btnGravar)) {
			try {
				FileDialog fd = new FileDialog((JDialog) null, "Salvar", FileDialog.SAVE);
				fd.setVisible(true);
				
				String nome = fd.getDirectory() + fd.getFile();
				
				FileOutputStream fo = new FileOutputStream(nome);
				ObjectOutputStream ofo = new ObjectOutputStream(fo);
				ofo.writeObject(alunos);
				ofo.close();
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(this, "Falha ao Gravar os Alunos em Arquivo");
				ex.printStackTrace();
			}
		} else if(botao.equals(btnCarregar)) {
			try {
				FileDialog fd = new FileDialog((JDialog) null, "Abrir", FileDialog.LOAD);
				fd.setVisible(true);
				
				String nome = fd.getDirectory() + fd.getFile();

				FileInputStream fo = new FileInputStream(nome);
				ObjectInputStream ofo = new ObjectInputStream(fo);
				alunos = (List<Aluno>)ofo.readObject();
				ofo.close();
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(this, "Falha ao Carregar os Alunos do Arquivo");
				ex.printStackTrace();
			}
		} else {
			System.exit(0);
		}
	}
}








