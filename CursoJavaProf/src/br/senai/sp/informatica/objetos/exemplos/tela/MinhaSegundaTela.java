package br.senai.sp.informatica.objetos.exemplos.tela;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.senai.sp.informatica.objetos.exemplos.jogos.Jogo;

@SuppressWarnings("serial")
public class MinhaSegundaTela extends JFrame implements ActionListener {
	private JTextField tfNome = new JTextField(20);
	private JTextField tfGenero = new JTextField(20);
	private JButton btSalvar = new JButton("Salvar");
	private JButton btListar = new JButton("Listar");
	private JButton btSair = new JButton("Sair");
	
	private List<Jogo> catalogo = new ArrayList<>();
	
	public MinhaSegundaTela() {
		JPanel pn1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
		pn1.add(new JLabel("Nome"));
		
		pn1.add(tfNome);
		
		add(pn1, BorderLayout.NORTH);

		JPanel pn2 = new JPanel(new FlowLayout(FlowLayout.LEADING));
		pn2.add(new JLabel("Gênero"));
		
		pn2.add(tfGenero);
		
		add(pn2, BorderLayout.CENTER);
		
		JPanel pnBotoes = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		btSalvar.addActionListener(this);
		pnBotoes.add(btSalvar);
		btListar.addActionListener(this);
		pnBotoes.add(btListar);
		btSair.addActionListener(this);
		pnBotoes.add(btSair);
		
		getRootPane().setDefaultButton(btSalvar);
		
		add(pnBotoes,BorderLayout.SOUTH);
		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	@Override
	public void actionPerformed(ActionEvent ev) {
		Object botao = ev.getSource();
	
		if(botao.equals(btSalvar)) {
			Jogo jogo = new Jogo();
			jogo.setNome(tfNome.getText());
			jogo.setGenero(tfGenero.getText());
			
			catalogo.add(jogo);
			
			tfNome.setText("");
			tfGenero.setText("");
			tfNome.requestFocus();
		} else if(botao.equals(btListar)) {
			String msg = "Catálogo de Jogos\n\n" +
					catalogo.stream()
						.map(Jogo::toString)
						.collect(Collectors.joining("\n"));
			JOptionPane.showMessageDialog(this, msg);
		} else {
			System.exit(0);
		}
	}
	
	public static void main(String[] args) {
		MinhaSegundaTela tela = new MinhaSegundaTela();
		tela.setVisible(true);
	}




}
