package br.senai.sp.informatica.objetos.exemplos.heranca;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import lombok.Data;


@Data
public abstract class Aluno implements Serializable {
	private static final long serialVersionUID = 1233L;
	
	private Integer matricula;
	private String nome;
	private String endereco;
	private LocalDate formatura;
	
	private static transient DateTimeFormatter fmt =
			DateTimeFormatter.ofPattern("EEEE, d 'de' MMMM 'de' yyyy");
//                                      segunda-feira, 23 de janeiro de 2019
	
	protected abstract String getTipo();
	
	@Override
	public String toString() {
		return "Matricula: " + matricula + 
				" Nome: " + nome + " End.: " + endereco + " Formatura: " +
				fmt.format(formatura) + "\n";
	}
}
