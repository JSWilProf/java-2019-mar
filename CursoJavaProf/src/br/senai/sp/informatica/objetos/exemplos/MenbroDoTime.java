package br.senai.sp.informatica.objetos.exemplos;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class MenbroDoTime extends Aluno {
	private Time meuTime;
	private String posicao;

	@Override
	public void adicionaNota(double nota) {
	}
}
