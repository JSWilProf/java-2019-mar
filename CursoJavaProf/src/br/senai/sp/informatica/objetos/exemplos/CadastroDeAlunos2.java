package br.senai.sp.informatica.objetos.exemplos;

import static br.senai.sp.informatica.lib.SwUtil.escreva;
import static br.senai.sp.informatica.lib.SwUtil.leInteiro;
import static br.senai.sp.informatica.lib.SwUtil.leReal;
import static br.senai.sp.informatica.lib.SwUtil.leTexto;

import java.util.ArrayList;
import java.util.List;

public class CadastroDeAlunos2 {
	public static void main(String[] args) {
		Curso oCurso = new Curso();
		oCurso.setNome(leTexto("Informe o Nome do Curso"));
		oCurso.setProfessor(leTexto("Informe o nome do Prof."));
		oCurso.setQtdHoras(leInteiro("Informe a qtd de horas"));
		oCurso.setAlunos(new ArrayList<>());
				
		String nome = leTexto("Informe o Nome do Aluno");
		while(!nome.equalsIgnoreCase("fim")) {
			// Criar um Aluno
			AlunoEspecial umAluno = new AlunoEspecial();
			// Atribuir o Nome
			umAluno.setNome(nome);
			// Atribuir suas Notas
			List<Double> notas = new ArrayList<>();
			for (int i = 0; i < 5/*notas.size()*/; i++) {
				notas.add(leReal(i+1,"ª nota"));
			}
			umAluno.setNotas(notas);
			// Matricular no Curso
			umAluno.setCurso(oCurso);
			
			oCurso.getAlunos().add(umAluno);
			
//			List<Aluno> alunos = oCurso.getAlunos();
//			alunos.add(umAluno);
			
			nome = leTexto("Informe o Nome do Aluno");
		}
		
		escreva(oCurso.toString());
	}
}
