package br.senai.sp.informatica.objetos.exemplos;

import java.util.List;

import lombok.Data;

@Data
public class Time {
	private String nome;
	private List<String> titulos;
}
