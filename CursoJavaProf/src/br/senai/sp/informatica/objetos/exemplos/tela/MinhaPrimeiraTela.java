package br.senai.sp.informatica.objetos.exemplos.tela;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MinhaPrimeiraTela {
	public static void main(String[] args) {
		JFrame tela = new JFrame("Catálogo de Jogos");
		
		JPanel pn1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
		pn1.add(new JLabel("Nome"));
		JTextField tfNome = new JTextField(20);
		pn1.add(tfNome);
		
		tela.add(pn1, BorderLayout.NORTH);

		JPanel pn2 = new JPanel(new FlowLayout(FlowLayout.LEADING));
		pn2.add(new JLabel("Gênero"));
		JTextField tfGenero = new JTextField(20);
		pn2.add(tfGenero);
		
		tela.add(pn2, BorderLayout.CENTER);
		
		JPanel pnBotoes = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
		JButton btSalvar = new JButton("Salvar");
		pnBotoes.add(btSalvar);
		
		JButton btListar = new JButton("Listar");
		pnBotoes.add(btListar);
		
		JButton btSair = new JButton("Sair");
		pnBotoes.add(btSair);
		btSair.addActionListener(ev -> System.exit(0));
		
		tela.getRootPane().setDefaultButton(btSair);
		
		tela.add(pnBotoes,BorderLayout.SOUTH);
		tela.pack();
		tela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		tela.setVisible(true);
	}
}
