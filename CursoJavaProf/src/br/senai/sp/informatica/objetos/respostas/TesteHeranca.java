package br.senai.sp.informatica.objetos.respostas;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class TesteHeranca {
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		CadFornecedor cadForn = new CadFornecedor();
		cadForn.setPreferredSize(new Dimension(200,200));
		
		JFrame frame = new JFrame();
		frame.setPreferredSize(new Dimension(200,200));

		if(cadForn instanceof JFrame) {
			JOptionPane.showMessageDialog(null, "é um JFrame");
		} else {
			JOptionPane.showMessageDialog(null, "não é um JFrame");
		}
		
		
		ActionListener listener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evento) {
				System.exit(0);
			}
		};
		
		ActionListener listener2 = (__) -> System.exit(0);
		
		if(cadForn instanceof ActionListener) {
			JOptionPane.showMessageDialog(null, "é um ActionListener");
			ActionListener listener3 = (ActionListener)cadForn;
			listener3.actionPerformed(null);
		} else {
			JOptionPane.showMessageDialog(null, "não é um ActionListener");
		}
	}
}
