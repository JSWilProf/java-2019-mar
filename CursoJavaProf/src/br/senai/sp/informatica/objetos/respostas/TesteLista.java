package br.senai.sp.informatica.objetos.respostas;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class TesteLista {
	public static void main(String[] args) {
		List<Fornecedor> lista = new ArrayList<>();
		
		
		Endereco end1 = new Endereco();
		end1.setLogradouro("Rua da Esquina");
		end1.setNumero("123");
		end1.setBairro("Centro");
		
		Fornecedor forn1 = new Fornecedor();
		forn1.setId(1);
		forn1.setNome("Forquilhas & Cia.");
		forn1.setEndereco(end1);

		lista.add(forn1);
		
		String texto = forn1 + "\nHash Code:" +forn1.hashCode();

		Endereco end2 = new Endereco("Av. dos Piratas", "99", "Vila Lá");
		
		Fornecedor forn2 = new Fornecedor(2, "Dell Computadores Ltda.", end2);
		
		
		texto += "\n\n" +
				forn2 + "\nHash Code:" +forn2.hashCode();
		
		
		lista.add(forn2);
		
				
//				new Fornecedor(
//					1,"Forquilhas & Cia.",
//					"Rua da Esquina, 123");
		
		
		Endereco end3 = Endereco.builder()
				.logradouro("Rua da Esquina")
				.numero("123")
				.bairro("Vila Sei lá Onde")
				.build();
			
		Fornecedor forn3 = Fornecedor.builder()
				.id(1)
				.nome("Forquilhas & Cia.")
				.endereco(end3)
				.build();
		
		JOptionPane.showMessageDialog(null,
				texto + "\n\n" +
				forn3 + "\nHash Code:" +forn3.hashCode());
		
		String msg = "Cadastro de Fornecedores\n\n";
		for (Fornecedor forn : lista) {
			msg += forn + "\n";
		}
		JOptionPane.showMessageDialog(null, msg);
		
		if(lista.contains(forn3)) {
			JOptionPane.showMessageDialog(null, "Encontrei");
		} else {
			JOptionPane.showMessageDialog(null, "Não encontrei");
		}

	}
}
