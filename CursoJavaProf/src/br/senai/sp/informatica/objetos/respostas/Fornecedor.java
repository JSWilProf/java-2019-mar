package br.senai.sp.informatica.objetos.respostas;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Fornecedor implements Comparable<Fornecedor> {
	private Integer id;
	private String nome;
	private Endereco endereco;
	
	@Override
	public String toString() {
		return "id: " + id + " nome: " + nome + " endereco: " + endereco;
	}

	@Override
	public int compareTo(Fornecedor outro) {	
		return nome.compareTo(outro.nome);
	}
}
