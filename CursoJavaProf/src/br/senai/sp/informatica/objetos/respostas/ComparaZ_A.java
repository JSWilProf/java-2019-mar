package br.senai.sp.informatica.objetos.respostas;

import java.util.Comparator;

public class ComparaZ_A implements Comparator<Fornecedor>{
	@Override
	public int compare(Fornecedor obj, Fornecedor outro) {
		return outro.getNome().compareTo(obj.getNome());
	}
}
