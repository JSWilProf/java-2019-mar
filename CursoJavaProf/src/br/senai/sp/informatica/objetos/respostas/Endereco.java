package br.senai.sp.informatica.objetos.respostas;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Endereco {
	private String logradouro;
	private String numero;
	private String bairro;
	
	@Override
	public String toString() {
		return logradouro + ", " + numero + " - " + bairro;
	}
	
	
}
