package br.senai.sp.informatica.api.sql.exemplos.produtos;

import java.io.Serializable;

import lombok.Data;

@Data
public class Produto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer idproduto;
	private int codigo;
	private String nome;
	private String descricao;
	private double valor;
	private int quantidade;
	
	public double getTotal() {
		return quantidade * valor;
	}
}
