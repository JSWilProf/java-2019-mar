package br.senai.sp.informatica.api.sql.exemplos.produtos;

import java.awt.EventQueue;
import java.awt.FileDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import br.senai.sp.informatica.lib.SwUtil;

@SuppressWarnings("serial")
public class CadProduto extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JLabel lblCod;
	private JTextField tfCod;
	private JLabel lblNome;
	private JTextField tfNome;
	private JLabel lblDescrio;
	private JTextField tfDescricao;
	private JLabel lblValor;
	private JTextField tfValor;
	private JLabel lblQuantidade;
	private JTextField tfQuantidade;
	private JScrollPane scrollPane;
	private JTable table;
	private JButton btnInserir;
	private JButton btnSalvar;
	private JButton btnSair;
	private JLabel lblTotalGeral;
	private JTextField tfTotal;

	private List<Produto> lista = new ArrayList<>();
	private ProdutoModel model = new ProdutoModel(lista);
	private NumberFormat fmt = NumberFormat.getNumberInstance(); 
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			@SuppressWarnings("unchecked")
			public void run() {
				try {
					CadProduto frame = new CadProduto();
					
					try {
						FileDialog dialogo = new FileDialog((JFrame)null, "Abrir", FileDialog.LOAD);
						dialogo.setVisible(true);
						
						String arquivo = dialogo.getDirectory() + dialogo.getFile();
						
						FileInputStream fileIn = new FileInputStream(arquivo);
						ObjectInputStream entrada = new ObjectInputStream(fileIn);
						frame.lista = (List<Produto>) entrada.readObject();
						
						frame.model.setLista(frame.lista);
						entrada.close();
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null, "Falha ao Carregar os Produtos");
					}
					
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public CadProduto() {		
		fmt.setMaximumFractionDigits(2);
		fmt.setMinimumFractionDigits(2);

		setTitle("Cadastro de Produtos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 351);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		lblCod = new JLabel("Cod.");
		
		tfCod = new JTextField();
		tfCod.setColumns(10);
		
		lblNome = new JLabel("Nome");
		
		tfNome = new JTextField();
		tfNome.setColumns(10);
		
		lblDescrio = new JLabel("Descrição");
		
		tfDescricao = new JTextField();
		tfDescricao.setColumns(10);
		
		lblValor = new JLabel("Valor");
		
		tfValor = new JTextField();
		tfValor.setColumns(10);
		
		lblQuantidade = new JLabel("Quantidade");
		
		tfQuantidade = new JTextField();
		tfQuantidade.setColumns(10);
		
		scrollPane = new JScrollPane();
		
		btnInserir = new JButton("Inserir");
		btnInserir.addActionListener(this);
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(this);
		
		btnSair = new JButton("Sair");
		btnSair.addActionListener(this);
		
		lblTotalGeral = new JLabel("Total Geral");
		
		tfTotal = new JTextField();
		tfTotal.setColumns(10);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE)
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addComponent(lblCod)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(tfCod, GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(lblNome)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tfNome, GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE))
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addComponent(lblDescrio)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(tfDescricao, GroupLayout.DEFAULT_SIZE, 338, Short.MAX_VALUE))
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addComponent(lblValor)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tfValor)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(lblQuantidade)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tfQuantidade, GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE))
						.addGroup(Alignment.TRAILING, gl_contentPane.createParallelGroup(Alignment.LEADING)
							.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
								.addComponent(lblTotalGeral)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(tfTotal, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(btnInserir)
								.addGap(18)
								.addComponent(btnSalvar)
								.addGap(18)
								.addComponent(btnSair))))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCod)
						.addComponent(tfCod, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNome)
						.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDescrio)
						.addComponent(tfDescricao, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblValor)
						.addComponent(tfValor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblQuantidade)
						.addComponent(tfQuantidade, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(tfTotal, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblTotalGeral))
					.addGap(14)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSair)
						.addComponent(btnSalvar)
						.addComponent(btnInserir)))
		);
		
		table = new JTable(model);
		scrollPane.setViewportView(table);
		contentPane.setLayout(gl_contentPane);
	}
	
	public void actionPerformed(ActionEvent evento) {
		Object botao = evento.getSource();
		
		if(botao.equals(btnInserir)) {
			try {
				Produto obj = new Produto();
				obj.setCodigo(Integer.parseInt(tfCod.getText()));
				obj.setNome(tfNome.getText());
				obj.setDescricao(tfDescricao.getText());
				obj.setValor(fmt.parse(tfValor.getText()).doubleValue());
				obj.setQuantidade(Integer.parseInt(tfQuantidade.getText()));
				
				lista.add(obj);
				// Avisa ao JTable que foi incluido um produto  
				model.fireTableDataChanged(); 
				
				SwUtil.limpa(this);
				tfCod.requestFocus();
				
				double total = 0;
				for (Produto produto : lista) {
					total += produto.getTotal();
				}
				tfTotal.setText(fmt.format(total));
			} catch (ParseException ex) {
				JOptionPane.showMessageDialog(this, "Valor é Inválido");
			}
		} else if(botao.equals(btnSalvar)) {
			try {
				FileDialog dialogo = new FileDialog(this, "Salvar", FileDialog.SAVE);
				dialogo.setVisible(true);
				
				String arquivo = dialogo.getDirectory() + dialogo.getFile();
				
				FileOutputStream fileOut = new FileOutputStream(arquivo);
				ObjectOutputStream saida = new ObjectOutputStream(fileOut);
				saida.writeObject(lista);
				saida.close();
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(this, "Falha na Gravação dos Produtos");
			}
		} else {
			System.exit(0);
		}
	}
}









