package br.senai.sp.informatica.api.sql.exemplos.produtos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class GravaProduto {
	public static void main(String[] args) {
		try {
			// Registra o Driver JDBC
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			// Conecta no Banco de Dados
			Connection con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3307/cursojava"
				+ "?useTimezone=true&serverTimezone=UTC&useSSL=false",
					"root","root132");
			
			// Obtem os dados para inserir no Banco
			Produto produto = new Produto();
			produto.setCodigo(143);
			produto.setNome("Blusa de Lã");
			produto.setDescricao("Blusa Amarela");
			produto.setValor(120);
			produto.setQuantidade(2);
			
			// Prepara a instrucao SQL
			PreparedStatement sql = con.prepareStatement(
					"INSERT INTO produto " + 
					"(codigo,nome,descricao,valor,quantidade) " + 
					"VALUES (?,?,?,?,?)");
			
			// Atribui os valores na instrucao SQL
			sql.setInt(1, produto.getCodigo());
			sql.setString(2, produto.getNome());
			sql.setString(3, produto.getDescricao());
			sql.setDouble(4, produto.getValor());
			sql.setInt(5, produto.getQuantidade());
			
			// Executa a instrucao SQL
			sql.execute();
			
			// Fechar a conexao com o Banco de Dados
			con.close();
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Driver JDBC não encontrado");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
