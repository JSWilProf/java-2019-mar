package br.senai.sp.informatica.api.sql.exemplos.produtos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class LeProduto {
	public static void main(String[] args) {
		try {
			// Registra o Driver JDBC
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			// Conecta no Banco de Dados
			Connection con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3307/cursojava"
				+ "?useTimezone=true&serverTimezone=UTC&useSSL=false",
					"root","root132");
		
			// Prepara a instrucao SQL
			PreparedStatement sql = con.prepareStatement(
					"select * from produto");

			List<Produto> lista = new ArrayList<>();
			
			// Le todos os registros do Banco de Dados
			ResultSet resultado = sql.executeQuery();
			while(resultado.next()) {
				Produto produto = new Produto();
				produto.setIdproduto(resultado.getInt("idproduto"));
				produto.setCodigo(resultado.getInt("codigo"));
				produto.setNome(resultado.getString("nome"));
				produto.setDescricao(resultado.getString("descricao"));
				produto.setValor(resultado.getDouble("valor"));
				produto.setQuantidade(resultado.getInt("quantidade"));
				
				lista.add(produto);
			}
			
			// Fecha a conexao com o Banco de Dados
		    con.close();
		    
		    String msg = "Produtos Cadastrados\n\n";
		    for (Produto produto : lista) {
				msg += produto + "\n";
			}
		    JOptionPane.showMessageDialog(null, msg);
		    
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Driver JDBC não encontrado");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
