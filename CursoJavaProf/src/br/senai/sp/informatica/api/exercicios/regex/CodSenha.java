package br.senai.sp.informatica.api.exercicios.regex;

public class CodSenha {

	public static void main(String[] args) {
		String[] senhas = new String[] { 
				"aSd5#09kdi72tgdsj",
				"kahjsefkjahsdflks",
				"9393938384slkdjfl"
		};

		/*
		 * Caracter especial
		 * Letra Maiúscula
		 * Números
		 * Tamanho mínimo de 8 caracteres
		 */

		for (String senha : senhas) {
			if(senha.length() < 8) {
				System.out.println("Senha Inválida");
				break;
			}
			
			boolean especial = false;
			boolean maiuscula = false;
			boolean numeros = false;
			
			for (int i = 0; i < senha.length(); i++) {
				char caracter = senha.charAt(i);
				if(!numeros)
				   numeros = Character.isDigit(caracter);
				if(!maiuscula)
					maiuscula = Character.isUpperCase(caracter);
				if(!especial) {
					especial = caracter == '#' || caracter == '&' ||
					           caracter == '$' || caracter == '%';
				}
			}
			if(numeros && especial && maiuscula) {
				System.out.println("Senha OK");
			} else {
				System.out.println("Senha Inválida");
			}
			
		}
	}
}
