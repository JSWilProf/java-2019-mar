package br.senai.sp.informatica.api.exercicios.regex;

public class CodProduto {
	/*
	 * 003-X/19 023/19 23-A/19 1123-C/19
	 */

	public static void main(String[] args) {
		String[] codigos = new String[] { 
				"003-X/19", // 0
				"023/19",   // 1
				"23-A/19",  // 2
				"1123-C/19" // 3
		};

		String mascara = "[0-9]{1,4}(-[A-Z])?/[0-9]{2}";

		for (String cod : codigos) {
			System.out.print("Cod: " + cod + " ");
			
			if(cod.matches(mascara)) {
				System.out.println("OK");
			} else {
				System.out.println("INVÁLIDO");
			}
		}
	}
}
