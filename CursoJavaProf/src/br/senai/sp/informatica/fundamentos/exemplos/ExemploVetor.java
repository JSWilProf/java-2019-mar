package br.senai.sp.informatica.fundamentos.exemplos;

import static br.senai.sp.informatica.lib.SwUtil.escreva;
import static br.senai.sp.informatica.lib.SwUtil.leInteiro;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ExemploVetor {
	public static void main(String[] args) {
//		double[][] tabela = new double[3][];
//		tabela[0] = new double[5];
//		tabela[1] = new double[2];
//		tabela[2] = new double[3];
//
//		double[][] tabela2 = new double[3][5];
	
		int tamanho = leInteiro("Informe quantas notas vc quer cadastrar?");
		
		int[] notas = new int[tamanho];
		int indice = 0;
		while(indice < notas.length) {
			notas[indice] = leInteiro("Informe a ", indice + 1, "ª nota");
			indice++;
		}
		
		String msg = "Notas Cadastradas\n\n";

		msg += "Utilizando o For\n";
		for (int i = 0; i < notas.length; i++) {
			int aNota = notas[i];
			msg += aNota + "\n";
		}
		
		msg += "\nUtilizando ForEach\n";
		for (int aNota : notas) {
			msg += aNota + "\n";
		}

		msg += "\nUtilizando Stream\n";
		msg += Arrays.stream(notas)
				.mapToObj(String::valueOf)
				.collect(Collectors.joining("\n"));
			
		escreva(msg);
	}
}
