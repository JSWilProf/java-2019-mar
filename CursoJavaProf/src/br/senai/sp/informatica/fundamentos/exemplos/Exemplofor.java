package br.senai.sp.informatica.fundamentos.exemplos;

import br.senai.sp.informatica.lib.SwUtil;

public class Exemplofor {
	public static void main(String[] args) {
		int inicio = 1;
		int termino = 4;
		
		String msg = "";
		
		for (int i = inicio; i <= termino; i++) {
			msg += i + "\n";
		}
		
	    SwUtil.escreva(msg);
	}
}
