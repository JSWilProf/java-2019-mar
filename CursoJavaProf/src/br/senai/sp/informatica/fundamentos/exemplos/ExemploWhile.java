package br.senai.sp.informatica.fundamentos.exemplos;

import br.senai.sp.informatica.lib.SwUtil;

public class ExemploWhile {
	public static void main(String[] args) {
		int num = SwUtil.leInteiro("Informe um nº");
		int cnt = 1;
		
		String msg = "";
		
		while(num > 0) {		
			msg += cnt++ + " " + num + "\n";
			
			num = SwUtil.leInteiro("Informe um nº");
		}
		SwUtil.escreva(msg);
	}
}
