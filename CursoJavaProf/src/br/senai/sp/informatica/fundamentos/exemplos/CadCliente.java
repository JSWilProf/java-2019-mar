package br.senai.sp.informatica.fundamentos.exemplos;

import javax.swing.JOptionPane;

public class CadCliente {
	public static void main(String[] args) {
		Cliente novoCliente = new Cliente();

		novoCliente.setNome(JOptionPane.showInputDialog("Informe o Nome"));
		novoCliente.setEmail(JOptionPane.showInputDialog("Informe o E-Mail"));
		
		String temp = JOptionPane.showInputDialog("Informe o Idade");
		novoCliente.setIdade(Integer.parseInt(temp));
		
		JOptionPane.showMessageDialog(null, novoCliente);
	}
}
