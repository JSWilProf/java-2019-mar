package br.senai.sp.informatica.fundamentos.exemplos;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Conversao {
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		float i = 10.9f;
		System.out.println(i);
		
		double j = i;
		System.out.println(j);
		
		int k = (int)j; 
		System.out.println(k);

		double r = (2 + 3)/3.0;
		
		String x = String.valueOf(r);
		
		System.out.println("Total:" + r);

		BigDecimal soma = new BigDecimal("2").add(new BigDecimal("3"));
		System.out.println(soma.toString());
		BigDecimal big = soma.divide(new BigDecimal("3"), 16, RoundingMode.CEILING);
		System.out.println(big.toString());
	}
}
