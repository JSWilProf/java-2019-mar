package br.senai.sp.informatica.fundamentos.respostas.ex03;

import br.senai.sp.informatica.lib.SwUtil;

public class ExFun01a {
	public static void main(String[] args) {
		int num1 = SwUtil.leInteiro("Informe o 1º nº");
		int num2 = SwUtil.leInteiro("Informe o 2º nº");
		
		testeIgualdade(num1, num2);
	}
	
	public static void testeIgualdade(int num1, int num2) {
		if(num1 == num2) {
			SwUtil.escreva("São Iguais");
		} else {
			SwUtil.escreva("São Diferentes");
		}
	}
}
