package br.senai.sp.informatica.fundamentos.respostas.extraii;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class Ex02a {
	public static void main(String[] args) {
		
		int nota1 = leNota("Informe a 1ª nota");
		int nota2 = leNota("Informe a 2ª nota");
		
		double media = (nota1 +  nota2) / 2;
		
		if(media <= 60) {
			escreva("Insuficiente");
		} else if(media <= 80) {
			escreva("Satisfatória");
		} else if(media <= 90) {
			escreva("Boa");
		} else {
			escreva("Excelente");
		}
	}
	
	public static int leNota(String texto) {
		int nota;
		
		for(;;) {
			nota = leInteiro(texto);
			
			if(nota < 0 || nota > 100) {
				escreva("Nota Inválida, informe novamente");
			} else {
				break;
			}
		}
		return nota;
	}
}
