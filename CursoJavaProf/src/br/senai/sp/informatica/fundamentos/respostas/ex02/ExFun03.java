package br.senai.sp.informatica.fundamentos.respostas.ex02;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class ExFun03 {
	public static void main(String[] args) {
		int saque = leInteiro("Informe o valor do Saque");

		String msg = "Notas de 100: " + saque / 100;
		saque %= 100;
		msg += "\nNotas de 50: " + saque / 50;
		saque %= 50;
		msg += "\nNotas de 20: " + saque / 20;
		saque %= 20;
		msg += "\nNotas de 10: " + saque / 10;
		saque %= 10;
		msg += "\nNotas de 5: " + saque / 5;
		saque %= 5;
		msg += "\nNotas de 2: " + saque / 2;
		saque %= 2;
		msg += "\nNotas de 1: " + saque;

		escreva(msg);
	}
}
