package br.senai.sp.informatica.fundamentos.respostas.ex03;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class ExFun01c {
	public static void main(String[] args) {
		int num1 = leInteiro("Informe o 1º nº");
		int num2 = leInteiro("Informe o 2º nº");
		
		String msg = num1 == num2 ? "São Iguais" : "São Diferentes";
		
		escreva(msg);	
	}
}
