package br.senai.sp.informatica.fundamentos.respostas.ex02;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class ExFun02a {
	public static void main(String[] args) {
		int inv = 0;
		int num = leInteiro("Informe um nº");

		while (num > 0) {
			inv = inv * 10 + num % 10;
			num /= 10;
		}

		escreva("num:", num, "\ninv:", inv);
	}
}
