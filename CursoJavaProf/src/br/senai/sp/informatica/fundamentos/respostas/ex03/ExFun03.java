package br.senai.sp.informatica.fundamentos.respostas.ex03;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class ExFun03 {
	public static void main(String[] args) {
		int num1 = leInteiro("Informe o 1º nº");
		int num2 = leInteiro("Informe o 2º nº");
		
		if(num1 % num2 == 0) {
			escreva("É divisível");
		} else {
			escreva("Não é divisível");
		}
			
	}
}
