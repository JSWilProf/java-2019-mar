package br.senai.sp.informatica.fundamentos.respostas.ex03;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class ExFun06 {
	public static void main(String[] args) {
		int idade = leInteiro("Informe a Idade");
		
		if(idade <= 10) {
			escreva("Infantil");
		} else if(idade <= 15) {
			escreva("Infanto");
		} else if(idade <= 18) {
			escreva("Juvenil");
		} else {
			escreva("Adulto");
		}
	}
}
