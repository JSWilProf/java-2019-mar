package br.senai.sp.informatica.fundamentos.respostas.ex04;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class ExFun01a {
	public static void main(String[] args) {
		String msg = "Números Ímpares entre ";
		
		int num1 = leInteiro("Informe o 1º nº");
		int num2 = leInteiro("Informe o 2º nº");
		
		if(num1 > num2) {
			int aux = num1;
			num1 = num2;
			num2 = aux;
		}
		
		msg += num1 + " e " + num2 + "\n\n"; 
		
		if(num1 % 2 == 0) num1++;
		
		for (;num1 <= num2; num1+=2) {
			msg += num1 + "\n";
		}
		
		escreva(msg);
	}
}
