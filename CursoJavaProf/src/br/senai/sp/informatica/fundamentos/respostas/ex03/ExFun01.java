package br.senai.sp.informatica.fundamentos.respostas.ex03;

import br.senai.sp.informatica.lib.SwUtil;

public class ExFun01 {
	public static void main(String[] args) {
		int num1 = SwUtil.leInteiro("Informe o 1º nº");
		int num2 = SwUtil.leInteiro("Informe o 2º nº");
		
		if(num1 == num2) {
			SwUtil.escreva("São Iguais");
		} else {
			SwUtil.escreva("São Diferentes");
		}
			
	}
}
