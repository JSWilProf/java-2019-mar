package br.senai.sp.informatica.fundamentos.respostas.ex04;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class ExFun04 {
	public static void main(String[] args) {
		String msg = "";
		
		int num = leInteiro("Informe um nº");
		int oMenor = num;
		int contador = 0;
		
		while (num > 0) {
			if(num < oMenor) {
				oMenor = num;
				contador = 1;
			} else if(num == oMenor) {
				contador++;
			}
			
			msg += num + " ";
			
			num = leInteiro("Informe um nº");
		}
		escreva("O menor nº é: ", oMenor,
				"\ne foi informado ", contador, " vezes",
				"\ndentre estes nºs informados\n", msg);
	}
}
