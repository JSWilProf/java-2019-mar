package br.senai.sp.informatica.fundamentos.respostas.ex02;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class ExFun02 {
	public static void main(String[] args) {
		int inv = 0;
		int num = leInteiro("Informe um nº");
		
		inv = inv * 10 + num % 10;
		num /= 10;

		inv = inv * 10 + num % 10;
		num /= 10;
		
		inv = inv * 10 + num % 10;
		num /= 10;
		
		escreva("num:", num, "\ninv:", inv);
	}
}
