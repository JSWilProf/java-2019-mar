package br.senai.sp.informatica.fundamentos.respostas.ex01;


import javax.swing.JOptionPane;

public class EXFun02d {
	public static void main(String[] args) {
		double media = 0;
		
		for (int i = 1; i <= 4; i++) {
			String aux = JOptionPane.showInputDialog("Informe o "+i+"º nº");
			media +=  Integer.parseInt(aux);
		}
		media /= 4;
		
		JOptionPane.showMessageDialog(null, "a média é: " + media);
	}
}
