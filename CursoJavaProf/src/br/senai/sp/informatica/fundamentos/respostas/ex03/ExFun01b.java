package br.senai.sp.informatica.fundamentos.respostas.ex03;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class ExFun01b {
	public static void main(String[] args) {
		int num1 = leInteiro("Informe o 1º nº");
		int num2 = leInteiro("Informe o 2º nº");
		
		escreva(testeIgualdade(num1, num2) ? "São Iguais" : "São Diferentes");
	}
	
	public static boolean testeIgualdade(int num1, int num2) {
		return num1 == num2;
	}
}
