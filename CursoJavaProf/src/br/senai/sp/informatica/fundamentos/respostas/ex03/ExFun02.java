package br.senai.sp.informatica.fundamentos.respostas.ex03;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class ExFun02 {
	public static void main(String[] args) {
		int num1 = leInteiro("Informe o  nº");
		
		if(num1 % 2 == 0) {
			escreva("É par");
		} else {
			escreva("É impar");
		}
			
	}
}
