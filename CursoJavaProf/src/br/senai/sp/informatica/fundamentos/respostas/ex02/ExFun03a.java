package br.senai.sp.informatica.fundamentos.respostas.ex02;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class ExFun03a {
	public static void main(String[] args) {
		int[] notas = { 100, 50, 20, 10, 5, 2, 1};
		String msg = "";
		int saque = leInteiro("Informe o valor do Saque");

		for (int i = 0; i < notas.length; i++) {
			msg += "Notas de " + notas[i] + ": " + saque / notas[i] + "\n";
			saque %= notas[i];
		}

		escreva(msg);
	}
}
