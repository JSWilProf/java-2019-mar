package br.senai.sp.informatica.fundamentos.respostas.ex01;


import javax.swing.JOptionPane;

public class EXFun02 {
	public static void main(String[] args) {
		int num1 = ExFun01a.leInteiro("Informe o 1º nº");
		int num2 = ExFun01a.leInteiro("Informe o 2º nº");
		int num3 = ExFun01a.leInteiro("Informe o 3º nº");
		int num4 = ExFun01a.leInteiro("Informe o 4º nº");
		
		double media =(num1 + num2 + num3 + num4)/ 4.0;
		
		JOptionPane.showMessageDialog(null, "a média é: " + media);
	}
}
