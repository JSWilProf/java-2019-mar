package br.senai.sp.informatica.fundamentos.respostas.ex03;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class ExFun04 {
	public static void main(String[] args) {
		double salario = leReal("Informe o Salário");
		
		if(salario <= 300) {
			salario = salario - salario * 5.0 / 100;
		} else if(salario <= 1200) {
			salario = salario - salario * 0.1;
		} else {
			salario *= 0.85;
		}
		
		escreva("O Salário Líquido é de: R$ ", salario);
	}
}
