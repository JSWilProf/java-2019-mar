package br.senai.sp.informatica.fundamentos.respostas.ex03;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class ExFun02a {
	public static void main(String[] args) {
		int num1 = leInteiro("Informe o  nº");
		
		String msg = "É ";
		
		if(num1 % 2 != 0) {
			msg += "im";
		}
		
		escreva(msg , "par");
	}
}
