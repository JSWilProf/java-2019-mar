package br.senai.sp.informatica.fundamentos.respostas.ex01;

import javax.swing.JOptionPane;

public class ExFun01a {
	public static void main(String[] args) {
		int num1 = leInteiro("Informe o 1º nº");
		int num2 = leInteiro("Informe o 2º nº");
		
		double resultado = Math.pow(num1, num2);
		
		JOptionPane.showMessageDialog(null, "O Total é: " + resultado);
	}
	
	public static int leInteiro(String mensage) {
		String aux = JOptionPane.showInputDialog(mensage);
		return Integer.parseInt(aux);	
	}
}
