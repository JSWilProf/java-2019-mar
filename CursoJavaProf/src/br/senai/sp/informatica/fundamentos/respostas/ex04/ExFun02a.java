package br.senai.sp.informatica.fundamentos.respostas.ex04;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class ExFun02a {
	public static void main(String[] args) {
		String msg = "Números informados:\n";
		
		int qtd = leInteiro("Informe a quantidade de nº a ser informado");
		int oMaior = leInteiro("Informe o 1º nº");
		msg += oMaior + " ";
		
		for (int i = 1; i < qtd; i++) {
			int num = leInteiro("Informe o ", i + 1, "º nº");
			
			if(num > oMaior) {
				oMaior = num;
			}
			
			msg += num + " ";
		}
		escreva(msg, "\nO maior nº é: ", oMaior);
	}
}
