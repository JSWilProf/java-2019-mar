package br.senai.sp.informatica.fundamentos.respostas.ex02;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class ExFun01 {
	public static void main(String[] args) {
		double largura = leReal("Informe a Largura");
		double comprimento = leReal("Informe a Comprimento");
		double profundidade = leReal("Informe a Profundidade");
		escreva("O preço final é de R$", 
				largura * comprimento * profundidade * 45);
	}
}
