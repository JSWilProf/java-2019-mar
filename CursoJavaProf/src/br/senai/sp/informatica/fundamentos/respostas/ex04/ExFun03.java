package br.senai.sp.informatica.fundamentos.respostas.ex04;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class ExFun03 {
	public static void main(String[] args) {
		double sal = leReal("Informe o Salário");
		
		while(sal > 0) {
			double salNovo;
			if(sal <= 500) {
				salNovo = sal * 1.2;
			} else {
				salNovo = sal * 1.1;
			}
			escreva("Seu sal.eario é de R$", sal,
					"\ne com aumento fica R$ ", salNovo);
			
			sal = leReal("Informe o Salário");
		}
	}
}
