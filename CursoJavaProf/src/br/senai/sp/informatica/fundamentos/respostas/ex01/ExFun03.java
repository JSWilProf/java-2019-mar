package br.senai.sp.informatica.fundamentos.respostas.ex01;

import javax.swing.JOptionPane;

public class ExFun03 {
	public static void main(String[] args) {
		String aux = JOptionPane.showInputDialog("Informe a base");
		double base = Double.parseDouble(aux);	
		
		aux = JOptionPane.showInputDialog("Informe a altura");
		double altura = Double.parseDouble(aux);	
		
		double area = base * altura / 2;
		
		
		String msg = String.format("A Área é de: %,.2f" , area);
		
		JOptionPane.showMessageDialog(null, msg);
	}
}
