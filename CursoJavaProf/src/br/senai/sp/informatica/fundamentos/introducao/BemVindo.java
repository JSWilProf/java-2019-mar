package br.senai.sp.informatica.fundamentos.introducao;

import javax.swing.JOptionPane;

public class BemVindo {
  public static void main(String[] args) {
    String nome = JOptionPane.showInputDialog("Digite seu nome");
    JOptionPane.showMessageDialog(null, "Bem Vindo, " + nome);
  }
}