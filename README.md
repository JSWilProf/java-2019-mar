# Repositório da Turma Java Fundamentos 2019 Março - Senai Informatica 1.32
## Bem Vindo
Aqui são disponibilizados os projetos Java Fundamentos, as apresentações, exercícios e respostas.
## Como obter
Para obter uma cópia deste conteúdo basta utilizar o comando:

Também é possível fazer o download através do link
[Java Fundamentos 2019 Março](http://localhost:9080/JSWilProf/Java-2019-mar)

# Ementa

## Programação do Curso (60h)

### Lógica de Programação

- Tipos de dados e palavras chaves
- Instruções Condicionais
- Laços de Repetição
- Vetores, Matrizes

### Orientação à Objetos

- Classes (Classes concretas, Classes abstratas, Classes anônimas, Classes internas, Classes genéricas)
- Objetos, Atributos, Métodos e Encapsulamento
- Subscrição e Sobreposição de atributos e métodos
- Herança, Agregação e Composição
- Interfaces, Interfaces Funcionais, Enumerações e expressões Lamba
- Tratamento de Exceções

### API - Application Program Interface

- Coleções de dados
- Manipulação de arquivos
- Expressão regular
- Acesso a bando de dados utilizando JDBC
- Interface Gráfica com Swing
- Lombok
